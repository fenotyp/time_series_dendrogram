#!/usr/bin/env python3.5


from scipy.cluster.hierarchy import dendrogram, linkage
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec
from dtw import dtw
import pandas as pd
'''
from math import sin
from math import pi
from math import cos
'''

def linkagetimeseries(X, method='DTW',Y=[], length=0):
    """
    X is a table of clusters
    """
    if method == 'DTW':
        diff = 1000 #np.max(np.max(X))
        for i in range(len(X)):
            for j in range(len(X[i])):

                for m in range(i+1,len(X)):
                    for n in range(len(X[m])):
                        dist, cost, acc, path = dtw(X[i][j], X[m][n], dist=lambda x, y: np.linalg.norm(x - y, ord=1))
                        if dist <= diff: #np.abs(X[i][j] - X[m][n]) <= diff:
                            diff = dist
                            a = i
                            b = m

        cluster = X[a][:]+X[b][:]
        #print(cluster)
        X.append(cluster)
        X[a] = []
        X[b] = []

        Y.append([a, b, diff, len(cluster)])
        print("STEP: ",len(Y),"/", length)
        if all(x==[] for x in X[:-1]): # if i is empty list -> (True)
            return np.array(Y)
        else:
            #print("drugie L: ", length)
            return linkagetimeseries(X, method='DTW', Y=Y, length=length)

    else:
        print("Algorithm performe standard linkage from scipy.cluster.hierarchy package")
        return linkage(X, method='single')
# end linkagetimeseries


class list_of_functions():
    def __init__(self, file_name):
        self.df = pd.read_csv(file_name, sep=",")
        #self.df.set_index('index')
        print(self.df.index)

    def head(self, nb):
        print(self.df.head(nb))

    def shape(self):
        print(self.df.shape)

    def index(self):
        return self.df.index

    def convert(self, size=None):
        #adjust data
        self.size = size
        self.X = []

        if type(self.size) == int:
            if self.size > self.df.shape[0]:
                print("Size is to large, therefore max value is used")
                self.size = int(self.df.shape[0])
        elif self.size == None:
            self.size = int(self.df.shape[0])


        for i in range(self.size):
            self.X.append([np.array(self.df.ix[i].values).reshape(-1, 1)]) # ! place to make shorter functions

    def clustering(self):
        return linkagetimeseries(self.X, method='DTW',length = len(self.X)-1)

    def series(self, i):
        return self.df[i]

    def __len__(self):
        return len(self.df)

    def __call__(self, i):
        return self.df[i]

#df = pd.read_csv("total.csv",sep=",")
data = list_of_functions("test1.csv")
data.head(5)
data.shape()
data.convert(100) #at the moment 600
R=data.clustering()
#print(R)


def make_ticklabels_invisible(fig):
    for i, ax in enumerate(fig.axes):
        if i != 0:
            ax.tick_params(labelbottom=False, labelleft=False)
            ax.set_xticks([])
            ax.set_yticks([])


fig = plt.figure()
fig.suptitle('bold figure suptitle', fontsize=14, fontweight='bold')


print("DLUGOSC", len(data))
print(data.index(), data.index()[0])
gs = GridSpec(len(data), 3)
ax1 = plt.subplot(gs[:, :2])
dendrogram(R, leaf_rotation=0, orientation='left', labels=data.index())
ax1.tick_params(labelsize=8)

# identical to ax1 = plt.subplot(gs.new_subplotspec((0, 0), colspan=3))

palete = [(0.3, 0.7, 0.9),(0.3, 0.7, 0.3),
          (0.6, 0.7, 0.9),(0.6, 0.7, 0.3),
          (0.9, 0.7, 0.9),(0.9, 0.7, 0.3)]
for i in range(len(data)):
    plt.subplot(gs[i, 2])
    print("U",int((i-i%3)/3))
    plt.plot(data.df.ix[i].values, color=palete[int((i-i%3)/3)])


fig.suptitle("Dendrogram for Time Series")
make_ticklabels_invisible(fig)

plt.show()




"""
# EXAMPLE FOR PRINTING

df = pd.read_csv("total.csv",sep=",")
#print(df.head(5))
#print(df.shape[1])

A = df.ix[0].values
B = df.ix[4].values
C = df.ix[9].values

plt.figure(1)
plt.plot(A, '-o', label='x')
plt.plot(B, '-o', label='y')
plt.plot(C, '-o', label='z')
plt.legend(loc='upper left')

A = np.array(A).reshape(-1, 1)
B = np.array(B).reshape(-1, 1)
C = np.array(C).reshape(-1, 1)

dist, cost, acc, path = dtw(A, B, dist=lambda x, y: np.linalg.norm(x - y, ord=1))
print(dist#, cost#, acc, path)
plt.figure(2)
plt.imshow(acc.T, origin='lower', cmap=plt.cm.gray, interpolation='nearest')
plt.plot(path[0], path[1], 'w')
plt.xlim((-0.5, acc.shape[0]-0.5))
plt.ylim((-0.5, acc.shape[1]-0.5))

dist, cost, acc, path = dtw(A, C, dist=lambda x, z: np.linalg.norm(x - z, ord=1))
print(dist#, cost#, acc, path)
plt.figure(3)
plt.imshow(acc.T, origin='lower', cmap=plt.cm.gray, interpolation='nearest')
plt.plot(path[0], path[1], 'w')
plt.xlim((-0.5, acc.shape[0]-0.5))
plt.ylim((-0.5, acc.shape[1]-0.5))

dist, cost, acc, path = dtw(B, C, dist=lambda y, z: np.linalg.norm(y - z, ord=1))
print(dist#, cost#, acc, path)
plt.figure(4)
plt.imshow(acc.T, origin='lower', cmap=plt.cm.gray, interpolation='nearest')
plt.plot(path[0], path[1], 'w')
plt.xlim((-0.5, acc.shape[0]-0.5))
plt.ylim((-0.5, acc.shape[1]-0.5))

plt.show()
# END OF EXAMPLE
"""





'''
# linkage for tests
X = [[i] for i in [2.2, 8.1, 0.7, 4.8, 1.5, 9.77, 9.77, 0.2]]
print(X)
Z = linkage(X, method='single')
R = linkagetimeseries(X, method='DTW')
print(R)


print("\n\n")
print(Z)

plt.figure(figsize=(5, 2))
dendrogram(Z)
#plt.show()

plt.figure(figsize=(5, 2))
dendrogram(R)
plt.show()

'''