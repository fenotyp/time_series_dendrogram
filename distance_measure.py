# import local modules and distances
from kshape.core import kshape, zscore, _sbd
from ppa_sax.bop import *
from scipy.spatial import distance
from dtw import dtw
from lcs.core import *




if __name__ == "__main__":

    # implementation of Bag-of-Words problem
    shift = -120.42#-120.42
    up_limit = 300.0 #300
    mr = pd.read_csv("data_51.csv", sep=";")
    cl = pd.read_csv("tmp.csv", sep=";")
    print(mr.head(5))

    print(mr['time'].dtypes, mr['Y11'].dtypes,)

    measur = [[], []]
    calc =   [[], []]

    for i in xrange(len(mr['time'].values)):
        if mr['time'].values[i] + shift >= 0.0 and mr['time'].values[i] + shift <= up_limit:
            measur[0].append(mr['time'].values[i] + shift)
            measur[1].append(mr['Y11'].values[i])
        elif mr['time'].values[i] + shift > up_limit:
            break

    for i in xrange(len(cl['time'].values)):
        if cl['time'].values[i] >= 0.0 and cl['time'].values[i] <= up_limit:
            calc[0].append(cl['time'].values[i])
            calc[1].append(cl['Y11'].values[i])
        elif cl['time'].values[i] > up_limit:
            break
    # initialize parameters
    w = 15 # size of window: range/w
    omega = 3 # length of the word
    alpha = 3 # number of letters

    raw_data = []
    df = []
    for i in range(1): #!! 2
        a = len(measur[0]) / 2 #!!2
        b = len(calc[0]) / 2 #!!2
        A = paa(w, measur[0][i * a:(i + 1) * a], measur[1][i * a:(i + 1) * a], alpha)
        B = paa(w, calc[0][i * b:(i + 1) * b], calc[1][i * b:(i + 1) * b], alpha)
        plt.figure(0)
        plt.plot(measur[0][i * a:(i + 1) * a], measur[1][i * a:(i + 1) * a], 'r-')
        plt.plot(calc[0][i * b:(i + 1) * b], calc[1][i * b:(i + 1) * b], 'b-')


    # ##### Prepare Dictionary #########################
    dictionary = prepare_dictionary(omega, alpha)

    bop_dist = distance_measure(A, B, dictionary, omega, alpha)
    print "DISTANCE for BOP IS: ",  np.average(bop_dist)


    # PLOT average DATA ##############
    plt.figure(1)

    plt.plot(A[0], A[1], 'r-')
    plt.plot(B[0], B[1], 'b-')

    '''
    # REST POLOT FOR bAG OF PATTERN
    plt.figure(2)
    plt.plot(A[0], distance, 'r-')

    A.append([]) # dodaje index 3
    for i in xrange(len(A[0])):
        Vec = sax_hist(A[2][i], dictionary, omega, alpha)
        Vec_length = 0.0
        for j in xrange(len(Vec)):
            Vec_length += Vec[j]**2
        A[3].append(math.sqrt(Vec_length))

    B.append([]) # dodaje index 3
    for i in xrange(len(B[0])):
        Vec = sax_hist(B[2][i], dictionary, omega, alpha)
        Vec_length = 0.0
        for j in xrange(len(Vec)):
            Vec_length += Vec[j]**2
        B[3].append(math.sqrt(Vec_length))

    plt.figure(3)
    N = len(A[3])
    x = range(N)

    plt.bar(A[0], A[3], color="red")
    plt.bar(B[0], B[3], color="blue")
    '''

    # implementation of k-shape
    a, b = _sbd(A[1], B[1])
    print "DISTANCE from KSHAPE IS: ", a

    plt.figure(2)
    plt.plot(B[1], 'b-')
    plt.plot(b, 'y-')

    # implementation of correlation
    print "DISTANCE from CORRELATIONS IS: ", distance.correlation(A[1], B[1])
    print "DISTANCE from EUCLIDEAN IS: ", distance.euclidean(A[1], B[1])

    # implementation of DTW

    x = np.array(A[1]).reshape(-1, 1)
    y = np.array(B[1]).reshape(-1, 1)
    dist, cost, acc, path = dtw(x, y, dist=lambda x, y: np.linalg.norm(x - y, ord=1))
    print "DISTANCE from DTW IS: ", dist

    plt.figure(3)
    plt.imshow(acc.T, origin='lower', cmap=plt.cm.gray, interpolation='nearest')
    plt.plot(path[0], path[1], 'w')
    plt.xlim((-0.5, acc.shape[0] - 0.5))
    plt.ylim((-0.5, acc.shape[1] - 0.5))

    print "DISTANCE from LCS IS: ", float(lcs_con(A[1], B[1]))

    plt.show()