import pandas as pd
import numpy as np
import string
import math
from scipy.stats import norm
from matplotlib import pyplot as plt


def getrange(list):
    """
    Simple function calculate range of data
    :param list: input list
    :return: return range
    """
    return max(list)-min(list)
# end def getrange:

def normal_prob(x,mu,std):
    """
    Return probability for x in normal distribution
    :param x:
    :return: probability for x
    """
    prob = (1/math.sqrt(2*math.pi*math.pow(std,2)))*math.exp(-math.pow((x-mu),2)/(2*math.pow(std,2)))
    return prob


def convert_to_string(LIST, sequence):
    """
    Recive normalized List and sequence with predefined percitiles and return word based on those distribution
    :param LIST: Chunk of time series. (normalized)
    :param sequence: prepared sequence list of tuples: sequence[][0]: percentilies borders sequence[1] letters
    :param L: number of letters in sequence L < len(List)
    :param W: number of intervals
    :param B: list of averaged values
    :param C: list of letters converted from B
    :return: string for analyzed chunk of time series
    """
    L = len(sequence)
    W = 6 # dlugosc slow
    list_len = len(LIST)
    window = len(LIST)/float(W)
    border = window
    B = []
    sum = []

    # average function in window
    for i in xrange(list_len):
        if i <= border:
            sum.append(LIST[i])
        else:
           border += window
           B.append(np.average(sum))
           sum = []
    B.append(np.average(sum))

    # convert numbers to letters
    C = []
    for j in xrange(W):
        for i in xrange(1, L):
            if B[j] >= sequence[i-1][0] and B[j] < sequence[i][0]:
                C.append(sequence[i][1])
            elif B[j] >= sequence[-1][0]:
                C.append(sequence[-1][1])
                break

    return ''.join(C)



def sax_hist(patern, dictionary, omega=4, alpha=4):
    """
    Perform histogram calculation.
    Return SAX hitogram for one analyzed series if string is pased
    or
    Return SAX histogram for all analyzed time series
    :param patern: string or list of strings
    :param dictionary: dictionary for calculate ocurence in the passed pattern
    :param omega: length of the words in the dictionary
    :param alpha: number of leters
    :return: list of occurence
    """
    histogram = [0 for i in range(alpha ** omega)]
    if type(patern) == str:
        for i in xrange(alpha**omega):
            histogram[i] = patern.count(dictionary[i])
        return histogram

    elif type(patern) == list:
        for j in xrange(len(patern)):
            for i in xrange(alpha**omega):
                histogram[i] += patern[j].count(dictionary[i])
        return histogram



def paa(w=8,x=[],y=[],alpha = 4):
    """
    Calculate slid window and perform normalization
    :param w:
    :param t_range:
    :param x:
    :param y:
    """
    nb_points = 1000
    t_range = getrange(x)
    print "t_range: ",t_range
    OUT = [[] for i in xrange(3)]
    window = float(t_range)/float(w)
    print "window size: ", window
    patern1 = " "
    patern2 = " "
    z = np.linspace(x[0]+window/2.0,x[-1]-window/2.0,nb_points)
    for c in xrange(nb_points):
       ## print "STEP: ", c, "/",nb_points
        t1 = z[c] - window/2.0
        t2 = z[c] + window/2.0
        SUM = []
        for i in xrange(len(x)):
            if x[i] >= t1 and x[i] <= t2:
                SUM.append(y[i])
            elif x[i] > t2:
                break
        OUT[0]=z
        mean_SUM = np.average(SUM)
        std_SUM = np.std(SUM)

        # Normalize data
        NSUM = [(d - mean_SUM) / std_SUM for d in SUM]

        method = "normal"
        if method == "percentile":
            percenty = [min(NSUM)]+[np.percentile(NSUM, i * 100 / alpha) for i in range(1,alpha)]  # np.percentile([array],
            sequence = zip(percenty, string.ascii_letters)
            patern2 = convert_to_string(NSUM, sequence)
        elif method == "normal":
            percenty = [min(NSUM)]+[norm.ppf(float(i)/float(alpha)) for i in range(1,alpha)]
            sequence = zip(percenty, string.ascii_letters)
            patern2 = convert_to_string(NSUM, sequence)
        else:
            raise ValueError('Bad method for calculate word distribution')

        if patern1 == patern2:
            patern2 = " "
        else:
            patern1 = patern2 # save previous patern

        OUT[1].append(mean_SUM)
        OUT[2].append(patern2)
    return OUT


def prepare_dictionary(omega, alpha):
    """
    :param omega: length of the words in the dictionary
    :param alpha: number of leters
    :return: dictionary
    """

    dictionary = [0 for i in range(alpha ** omega)]
    # create dictionary
    for i in xrange(len(dictionary)):
        word = ''
        for a in xrange(omega):
                word += string.ascii_letters[i/alpha ** a % alpha]  #i % (alpha ** (a+1))

        dictionary[i] = word
    return dictionary


def distance_measure(A, B, dictionary, omega, alpha):
    """
    Performe distance measure
    :param A: Data series A
    :param B: Data series B
    :return: distance
    """
    distance = []
    if len(A[0]) == len(B[0]):
        for i in xrange(len(A[0])):
            Vec1 = sax_hist(A[2][i], dictionary, omega, alpha)
            Vec2 = sax_hist(B[2][i], dictionary, omega, alpha)
            Vec_distance = 0.0
            for j in xrange(len(Vec1)):
                Vec_distance += (Vec1[j]-Vec2[j])**2
            distance.append(math.sqrt(Vec_distance))
    else:
        raise ValueError('Cos jest nie tak z dlugosciami wektorow')

    return distance


if __name__ == "__main__":

    # implementation of Bag-of-Words problem
    shift = -120.42#-120.42
    up_limit = 100.0 #300
    mr = pd.read_csv("data_51.csv", sep=";")
    cl = pd.read_csv("tmp.csv", sep=";")
    print(mr.head(5))

    print(mr['time'].dtypes, mr['Y11'].dtypes,)

    measur = [[], []]
    calc =   [[], []]

    for i in xrange(len(mr['time'].values)):
        if mr['time'].values[i] + shift >= 0.0 and mr['time'].values[i] + shift <= up_limit:
            measur[0].append(mr['time'].values[i] + shift)
            measur[1].append(mr['Y11'].values[i])
        elif mr['time'].values[i] + shift > up_limit:
            break

    for i in xrange(len(cl['time'].values)):
        if cl['time'].values[i] >= 0.0 and cl['time'].values[i] <= up_limit:
            calc[0].append(cl['time'].values[i])
            calc[1].append(cl['Y11'].values[i])
        elif cl['time'].values[i] > up_limit:
            break
    # initialize parameters
    w = 15 # size of window: range/w
    omega = 3 # length of the word
    alpha = 3 # number of letters

    raw_data = []
    df = []
    for i in range(1): #!! 2
        a = len(measur[0]) / 2 #!!2
        b = len(calc[0]) / 2 #!!2
        A = paa(w, measur[0][i * a:(i + 1) * a], measur[1][i * a:(i + 1) * a], alpha)
        B = paa(w, calc[0][i * b:(i + 1) * b], calc[1][i * b:(i + 1) * b], alpha)
        plt.figure(0)
        plt.plot(measur[0][i * a:(i + 1) * a], measur[1][i * a:(i + 1) * a], 'r-')
        plt.plot(calc[0][i * b:(i + 1) * b], calc[1][i * b:(i + 1) * b], 'b-')

   ##     X = paa(w, measur[0][i * a:(i + 1) * a],
   ##             (measur[1][i * a:(i + 1) * a] - np.average(measur[1][i * a:(i + 1) * a])) / np.std(measur[1][i * a:(i + 1) * a]), alpha)
   ##     Y = paa(w, calc[0][i * b:(i + 1) * b],
   ##             (calc[1][i * b:(i + 1) * b] - np.average(calc[1][i * b:(i + 1) * b])) / np.std(calc[1][i * b:(i + 1) * b]), alpha)

    #OLD A = paa(w,col[0],col[1])

    # ##### Prepare Dictionary #########################
    dictionary = prepare_dictionary(omega, alpha)
    print "DICTIONARY: ", dictionary
    distance = distance_measure(A, B, dictionary, omega, alpha)

    print "DISTANCE IS: ", np.average(distance)
    # PLOT DATA ##############




    plt.figure(1)

    plt.plot(A[0], A[1], 'r-')
    plt.plot(B[0], B[1], 'b-')

    plt.figure(2)
    plt.plot(A[0], distance, 'r-')


    A.append([]) # dodaje index 3
    for i in xrange(len(A[0])):
        Vec = sax_hist(A[2][i], dictionary, omega, alpha)
        Vec_length = 0.0
        for j in xrange(len(Vec)):
            Vec_length += Vec[j]**2
        A[3].append(math.sqrt(Vec_length))

    B.append([]) # dodaje index 3
    for i in xrange(len(B[0])):
        Vec = sax_hist(B[2][i], dictionary, omega, alpha)
        Vec_length = 0.0
        for j in xrange(len(Vec)):
            Vec_length += Vec[j]**2
        B[3].append(math.sqrt(Vec_length))

    plt.figure(3)
    N = len(A[3])
    x = range(N)

    plt.bar(A[0], A[3], color="red")
    plt.bar(B[0], B[3], color="blue")




    plt.show()
