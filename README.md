
# Time_series_dendrogram

Program uses various distance measure betwen time series and perform 
dendrogram clustering.

## Used distances
DTW - Dynamic Time Warping  
LCS - Longest Common Subsequence  
ED - Euclidean Distance  
C - Correlations  
KS - k-shape  
BOP - Bag Of Patterns

## Results

![picture](https://previews.dropbox.com/p/thumb/AAJ2BP6JXIIPy2w5D2YagbdgtJ2WP5mfKgsCqXmVMKxrLGG3MWZ4EpMNR0YqNseClDh6bZQ8_nWAyrtMYVp_JhKoi3u0Ka5wCMZ1Ab0R8zmE7d2nQVUn52vfhs7-Oo7W-Neczcsj9XBE1YbSnMFuYP1UiQLNB1EaogRW0P42bEL0aRU3rZY8SHlW_d4TPPoLBKrYa_wjK9F3KJFg6m4xEwWvsY0BmyzyTf-yyEs6w6qmzg/p.png?size=2048x1536&size_mode=3)
![picture](https://previews.dropbox.com/p/thumb/AALjF7V_ZKVQ4VcWXdzMLk7Kj-_IZztDR8a_obWb1l3R9e4d_F_tCw_Hs48SuikRYmMtbLbQYmXC1XG1i0pFI8bcB6yRJPHHekpkqTtPG882YhcXM4gTOIHrfzbkPmr4rE35CB-XK3tBgLt1nxe7SaVAiy9YLrJMqOQM28gmhDeSeSy51xo8S06gwET2J6rTr_4dAzcXdaouZ5-yWvFKANEwr_1qsxrQ-SMs66sMHY7RTA/p.png?size=2048x1536&size_mode=3)
![picture](https://previews.dropbox.com/p/thumb/AAJ5Mu1MaYc4Roxf2SD2_I79lK9VLef9JVlGYa99Dk3PFvSfrNHwXc_LahjarRKNO5WsGCB25_1bftCwS70Qwnhlk5_t3jpo92oHdftcl5oF3rrdDyrw-zQI7fW5pgIf99771iWwWZPpDDd_Bj_T2NkTGL1M-npLn3NLP07jAucJAaP5QH2u50rsUp7Gu4u1TJVxmE9iY7r2z20eH274lYPUBcHdrzFVIVry7FVOaSsXAA/p.png?size=2048x1536&size_mode=3)
![picture](https://previews.dropbox.com/p/thumb/AALPnW9Kw71JgAsjq4qWDiLO1em8OhUieBWS434i87wCnkKLiIbKlHd3e1zk-tYv4CtLLeE8sCXZp5fvd83bfwqcjOOI5r1ErsLP2YB4nT2mlCiiE_GCp7Hp1wZYn1bGGCDPWiuJn_5i5tt3tngWVCAefP9YV9xxw6DLUwT44Ioc7V-XJIYCHWDLp9mknp-7VnCPlvIMNgVq-7aauR34Vpswo3TtOogJ7_bGicZOPf28Dw/p.png?size=2048x1536&size_mode=3)