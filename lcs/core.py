import pandas as pd
import numpy as np
import string
import math
from matplotlib import pyplot as plt

# Dynamic Programming implementation of LCS problem

def lcs(X, Y):
    # find the length of the strings
    m = len(X)
    n = len(Y)

    # declaring the array for storing the dp values
    L = [[None] * (n + 1) for i in xrange(m + 1)]

    """Following steps build L[m+1][n+1] in bottom up fashion
    Note: L[i][j] contains length of LCS of X[0..i-1]
    and Y[0..j-1]"""
    for i in range(m + 1):
        for j in range(n + 1):
            if i == 0 or j == 0:
                L[i][j] = 0
            elif X[i - 1] == Y[j - 1]:
                L[i][j] = L[i - 1][j - 1] + 1
            else:
                L[i][j] = max(L[i - 1][j], L[i][j - 1])

    # L[m][n] contains the length of LCS of X[0..n-1] & Y[0..m-1]
    return L[m][n]
# end of function lcs

def getrange(list):
    """
    Simple function calculate range of data
    :param list: input list
    :return: return range
    """
    return max(list)-min(list)


def paaa(w=8,x=[],y=[]):
    """
    Calculate slid window and perform normalization
    :param w:
    :param t_range:
    :param x:
    :param y:
    """
    nb_points = 500
    t_range = getrange(x)
    print "t_range: ",t_range
    OUT = [[] for i in xrange(2)]
    window = float(t_range)/float(w)
    print "window size: ", window
    z = np.linspace(x[0]+window/2.0,x[-1]-window/2.0,nb_points)
    for c in xrange(nb_points):
            t1 = z[c] - window/2.0
            t2 = z[c] + window/2.0
            SUM = []
            for i in xrange(len(x)):
                if x[i] >= t1 and x[i] <= t2:
                    SUM.append(y[i])
                elif x[i] > t2:
                    break
            OUT[0]=z
            OUT[1].append(np.average(SUM))
    print len(OUT[0]),len(OUT[1]), OUT[0][0]
    return OUT


def lcs_con(A, B):
    w = 30
    raw_data = []
    df = []
    LISTA = [A,B]
    measur = [[], []]
    calc = [[], [], []]

    print "=================================="

    M1 = []
    L = 15  # number of letters

    for k in LISTA:
        dict = [[], []]
        for i in range(L):
            dict[0].append(string.ascii_letters[i])
            dict[1].append(np.percentile(k[1], i * 100 / L))

        B = []
        for i in xrange(len(k)):
            for j in xrange(L - 1):
                if k[i] >= dict[1][j] and k[i] < dict[1][j + 1]:
                    B.append(dict[0][j])
                elif k[i] >= dict[1][-1]:
                    B.append(dict[0][-1])
                    break
        M1.append(B)
    M1 = [''.join(c) for c in M1]
    print M1
    return lcs(M1[0],M1[1])